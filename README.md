![status-archived](https://bitbucket.org/repo/ekyaeEE/images/3278295154-status_archived.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-closed](https://bitbucket.org/repo/ekyaeEE/images/1555006384-issues_closed.png)

# RATIONALE #

* This repository collect (a kind of quick list) all the settings and processes running and executing diverse functions inside the MacOSX environment with a focus in security and avoiding leaking, breaching personal data through our proxy server.
* Itself, this repository is related with this other [repo](https://bitbucket.org/imhicihu/conferences/src/master/)
* This repo is a living document that will grow and adapt over time
![waldoinfo.png](https://bitbucket.org/repo/ey5E4z8/images/3410292979-waldoinfo.png)

### What is this repository for? ###

* Quick summary
    - Blacklisting/whitelisting HTTP/HTTPS requests across proxy server(s)
* Version 1.01

### How do I get set up? ###

* Summary of set up
    - a personal computer running MacOSX operating system. That's includes a [hackintosh](https://en.wikipedia.org/wiki/Hackintosh) too!
* Configuration
    - [macOSX operating system](https://en.wikipedia.org/wiki/Macintosh_operating_systems)
	- [LuLu](https://objective-see.com/products/lulu.html): open-source firewall for macOS
	- [Hands Off!](https://www.oneperiodic.com/products/handsoff/): payware firewall for macOS
	- [LittleSnitch](https://www.obdev.at/products/littlesnitch/index.html): payware firewall for macOS
	- or for a detailed method/instructions vide [`colophon.md`](https://bitbucket.org/imhicihu/firewall-settings-on-mac-environments-internal-use/src/master/colophon.md)
* Dependencies
    - There is no NPM, Gulp or Brew implied
* How to run tests
    - See [`procedure.md`](https://bitbucket.org/imhicihu/firewall-settings-on-mac-environments-internal-use/src/master/Procedure.md)
* Deployment instructions
    - Follow the [instructions]((https://bitbucket.org/imhicihu/firewall-settings-on-mac-environments-internal-use/src/master/Procedure.md)) from every firewall to accept/deny connections
    
### Related repositories ###

* Some repositories linked with this project:
     - There is no related repository (up to now)

### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/firewall-settings-on-mac-environments-internal-use/src)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/firewall-settings-on-mac-environments-internal-use/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/firewall-settings-on-mac-environments-internal-use/commits/) section for the current status

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/firewall-settings-on-mac-environments-internal-use/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/firewall-settings-on-mac-environments-internal-use/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png) 